package com.umar;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration

public class config {
    @Bean
    @Lazy
    public Students getStudents(){
        return new Students();
    }

    @Bean
    @Lazy
    public Foods getFoods(){
        return new Foods();
    }

}



