package com.umar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

public class Foods {

    @Value("Banana")
    private String foodname;

    public Foods() {
        System.out.println("food bean is created");
    }

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    @Override
    public String toString() {
        return "foods{" +
                "foodname='" + foodname + '\'' +
                '}';
    }
}
