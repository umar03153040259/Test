package com.springJDBC;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);
        JdbcTemplate jdbcTemplate=(JdbcTemplate) context.getBean("jdbcTemplate");

        List<Student> list=jdbcTemplate.query("select * from stdtable",BeanPropertyRowMapper.newInstance(Student.class));
        System.out.println(list.toString());

    }
}
