package com.umar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Map;


public class Students {
    @Value("101")
    private int id;
    @Value("Umar")
    private String name;
    @Value("#{${course : {key1: '1', key2: '2'}}}")
    private Map<String,String> course;

    @Autowired
    @Lazy
    private Foods foods;

    public Students() {
        System.out.println("Stduent beantr creatd");
    }

    public Foods getFoods() {
        return foods;
    }

    public void setFoods(Foods foods) {
        this.foods = foods;
    }

    public Map<String, String> getCourse() {
        return course;
    }

    public void setCourse(Map<String, String> course) {
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Students{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", course=" + course +
                ", foods=" + foods +
                '}';
    }
}
